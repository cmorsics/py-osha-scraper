from lxml import html
import requests
from bs4 import BeautifulSoup
from subpartdef import SubPartDef

class SubPart(object):

        def __init__(self, url):
                self.url = 'https://www.osha.gov/'+url
                #print(self.url)
                self.subpartPage = requests.get(self.url)
                self.html = BeautifulSoup(self.subpartPage.text, "lxml")
                self.definitions = []
                
        def append_subpartdef(self, url):
                self.definitions.append(SubPartDef(url))
               