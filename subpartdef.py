from lxml import html
import requests
from bs4 import BeautifulSoup

class SubPartDef(object):

        def __init__(self, url):
                self.url = 'https://www.osha.gov/'+url
                #print(self.url)
                self.subpartPage = requests.get(self.url)
                self.html = BeautifulSoup(self.subpartPage.text, "lxml")
                self.regulationTableRows = self.html.find_all('table')[1].find_all('tr')
                print(self.__str__())
		
        def part_number(self):
                return self.getRowValue(0);
                
        def part_title(self):
                return self.getRowValue(1);
                
        def subpart(self):
                return self.getRowValue(2);
                
        def subpart_title(self):
                return self.getRowValue(3);
                
        def standard_number(self):
                return self.getRowValue(4);
                
        def title(self):
                return self.getRowValue(5);
                               
        def __str__(self):
                return "%s %s %s %s %s %s" % (
                        self.part_number(), 
                        self.part_title(), 
                        self.subpart(), 
                        self.subpart_title(),
                        self.standard_number(),
                        self.title()
                        )
                
        def sub_regulations(self):
                return [];
                
        def getRowValue(self,row):
                return self.regulationTableRows[row].find_all('td')[1].text
	