# py-osha-scraper
Web Scraper for OSHA Regulations

# Virtual Environment
* New virtual environment python 3
pyvenv venv

* use existing virtual environment
. venv/bin/activate

* stop
deactivate

* heirarchy
	Part
 	- ToC
 	- SubPart (Array of SubParts)
 		- SubPartDef (Array of SubPart Definitions)
