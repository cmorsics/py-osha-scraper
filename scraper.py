from lxml import html
import requests
from bs4 import BeautifulSoup
from regulation import Regulation
from part import Part

page = requests.get('https://www.osha.gov/pls/oshaweb/owasrch.search_form?p_doc_type=STANDARDS&p_toc_level=0&p_keyvalue=')
soup = BeautifulSoup(page.text, "lxml")

mainIndex = soup.find("div", class_="main")
for li in mainIndex.find('ul').find_all('li'):
	aTag = li.find_all('a')[0]
	#print(aTag)
	url='https://www.osha.gov/'+aTag['href']
	#print(url)
	#print(Part(url))
	part = Part(url)
	#reg = Regulation(url)
	#print(reg)
	break
	
